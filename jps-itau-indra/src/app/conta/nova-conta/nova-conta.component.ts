import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Cliente } from 'src/app/_model/cliente';
import { ContaService } from 'src/app/_services/conta.service';

@Component({
  selector: 'app-nova-conta',
  templateUrl: './nova-conta.component.html',
  styleUrls: ['./nova-conta.component.css']
})
export class NovaContaComponent implements OnInit {

  titulo: string = 'Abrir Conta';
  cliente: any = {};
  registerForm: FormGroup;

  constructor(
    private contaService: ContaService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() {
    this.validation();
  }

  validation() {
    this.registerForm = this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(100)]],
      cpf: ['', [Validators.required]],
      celular: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      dataNascimento: ['', [Validators.required]],
      passwords: this.fb.group({
        password: ['', [Validators.required, Validators.minLength(4)]],
        confirmPassword: ['', Validators.required]
      }, { validator: this.compararSenhas }),
      aceitaTermo: ['', Validators.required],
      receberInformacoes: ['']
    });
  }

  compararSenhas(fb: FormGroup) {
    const confirmSenhaCtrl = fb.get('confirmPassword');
    if (confirmSenhaCtrl.errors == null || 'mismatch' in confirmSenhaCtrl.errors) {
      if (fb.get('password').value !== confirmSenhaCtrl.value) {
        confirmSenhaCtrl.setErrors({ mismatch: true });
      } else {
        confirmSenhaCtrl.setErrors(null);
      }
    }
  }

  novaConta() {
    //this.contaService.postConta(this.cliente);

    var resp = this.contaService.postConta(this.cliente);
   
    if(resp.status == 'success'){
      this.router.navigate(['/login']);
      this.toastr.success('Conta criada.');
    } else {
      this.toastr.error('Falha ao tentar criar a conta.');
    }
  }

}
