import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ContaService } from 'src/app/_services/conta.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  titulo: string = 'Acesse sua conta';
  conta: any = {};

  constructor(
    public contaService: ContaService,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  login() {
    var resp = this.contaService.login(this.conta);
   
    if(resp.status == 'success'){
      this.router.navigate(['/home']);
      this.toastr.success('Acesso a conta realizado.');
    } else {
      this.toastr.error('Falha ao tentar logar.');
    }
    /*
      this.contaService.login(this.conta).subscribe(
        () => {
          this.router.navigate(['/home']);
          this.toastr.success('Acesso a conta realizado.');
          //console.log(teste);
        },
        erro => {
          this.toastr.error('Falha ao tentar logar.');
        }
      )
      */
  }

}
