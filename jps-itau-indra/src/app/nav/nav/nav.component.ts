import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(
    //public toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  acessar(){
    this.router.navigate(['/login']);
  }

  novaConta(){
    this.router.navigate(['/nova-conta']);
  }
}
