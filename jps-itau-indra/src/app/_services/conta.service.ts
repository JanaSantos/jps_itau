import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cliente } from '../_model/cliente';

@Injectable({
  providedIn: 'root'
})
export class ContaService {

  constructor(private http: HttpClient) { }

  baseURL = 'http://localhost:5000/api/conta/';

  mockRespost: any = {
    message: 'Sucesso',
    status: 'success'
  };

  mockRespostError: any = {
    message: 'Erro',
    status: 'error'
  };

  login(model: any) {
    /*
    return this.http
      .post(`${this.baseURL}login`, model).pipe(
        map((response: any) => {
          const conta = response;
        })
      );
    */
    return this.mockRespost;
    //return this.mockRespostError;
  }

  postConta(cliente: Cliente) {
    //return this.http.post(this.baseURL, cliente);
    return this.mockRespost;
    //return this.mockRespostError;
  }
}
