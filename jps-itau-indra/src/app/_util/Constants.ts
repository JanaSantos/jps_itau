export class Constants {
    static readonly DATE_FMT = 'dd/MM/yyy';
    static readonly DATE_TIME_FMT = `${Constants.DATE_FMT} hh:mm`;
    static readonly TIME_FMT = 'hh:mm';
}
