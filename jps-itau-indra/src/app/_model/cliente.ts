export class Cliente {
    nome: string;
    cpf: number;
    celular: string;
    email: string;
    dataNascimento: Date;
    password: string;
    aceitaTermo: boolean;
    receberInformacoes: boolean;
}
