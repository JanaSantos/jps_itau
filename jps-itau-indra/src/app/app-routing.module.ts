import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './conta/login/login.component';
import { NovaContaComponent } from './conta/nova-conta/nova-conta.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  //{ path: 'contatos', component: ContatoComponent },
  { path: 'home', component: HomeComponent},
  { path: 'nova-conta', component: NovaContaComponent},
  { path: 'login', component: LoginComponent},
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: '**', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
